#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Client


 python3 client.py <IPReg>:<puertoReg> <IPProxy>:<puertoProxy> <dirCliente> <dirServidorRTP> <tiempo> <fichero>
 ==============================================================================================================
• `<IPReg>` es la dirección IP de la máquina donde está ServidorRegistrar
• `<puertoReg>` es el puerto UDP donde está escuchando ServidorRegistrar
• `<IPProxy>` es la dirección IP de la máquina donde está ServidorProxy
• `<puertoProxy>` es el puerto UDP donde está escuchando ServidorProxy
• `<dirCliente>` es la dirección SIP del cliente, con la que se va a registrar en ServidorRegistrar.
• `<dirServidorRTP>` es la dirección SIP del servidor cn el que se va a iniciar una sesión para que nos envíe audio,
                     tal y como la haya registrado el servidor en cuestión. Las direcciones tendrán que ser direcciones SIP válidas,
                     por ejemplo `sip:heyjude@signasong.net`
• `<tiempo>` será el tiempo, en segundos, que el cliente mantendrá abierta la sesión una vez ésta haya empezado: al
             terminar este tiempo, el cliente enviará un `BYE` para terminar la sesión
• `<fichero>` será el fichero donde se guardará el audio recibido de ServidorRTP

Example: python3 client.py 127.0.0.1:6001 127.0.0.1:6002 sip:yo@clientes.net sip:heyjude@signasong.net 10 miaudio.mp3
"""
import logging
import socket
import socketserver
import sys
import threading
import time

logging.basicConfig(level=logging.INFO, format='%(asctime)s-%(message)s', datefmt='%Y%m%d%H%M%S')

# CONSTANTS
IP_SERVER = "127.0.0.1"
ERROR_MESSAGE = "Usage: python3 client.py <IPReg>:<portReg> <IPProxy>:<portProxy> <addrClient> <addrServerRTP> <time> " \
                "<file>"
ENCODING = "UTF-8"
LOGGING_FMT = '%(asctime)s-%(message)s'
LOGGING_DATE_FMT = '%Y%m%d%H%M%S'
SIP_CONSTANT = "SIP/2.0"
REGISTER_COMMAND = "REGISTER"
CLIENT_AS_SERVER_PORT = 34543

# SIP METHODS
CLIENT_SERVER_SIP_METHODS = ["INVITE", "ACK", "BYE"]

"""
• v: versión, por defecto 0
• o: origen e identificador de sesión: la dirección del origen y su IP
• s: nombre de la sesión, que será el nombre del usuario SIP con el que se comunica
• t: tiempo que la sesión lleva activa, en nuestro caso, siempre 0
• m: tipo de elemento multimedia y puerto de escucha y protocolo de transporte
     utilizados, en esta práctica audio, el número de puerto pasado al programa
     principal como parámetro y RTP
     
Example:

INVITE sip:heyjude@signasong.net SIP/2.0
Content-Type: application/sdp

v=0
o=sip:yo@clientes.net 127.0.0.1
s=heyjude
t=0
m=audio 34543 RTP
"""


def get_application_parameters():
    try:
        logging.debug("Get Application Parameters - Init")
        register_server_ip, register_server_port = sys.argv[1].split(":")
        proxy_server_ip, proxy_server_port = sys.argv[2].split(":")
        dir_client = sys.argv[3]
        dir_servidor_rtp = sys.argv[4]
        time_expire = sys.argv[5]
        audio_file = sys.argv[6]
        logging.debug(f"Application Parameters: "
                      f"{register_server_port=}, "
                      f"{register_server_port=},"
                      f"{proxy_server_ip},"
                      f"{proxy_server_port},"
                      f"{dir_client},"
                      f"{dir_servidor_rtp},"
                      f"{time_expire},"
                      f"{audio_file}")
        logging.debug("Get Application Parameters - Finish Without Error")
        return register_server_ip, int(register_server_port), proxy_server_ip, int(proxy_server_port), \
               dir_client, dir_servidor_rtp, int(time_expire), audio_file
    except Exception as e:
        logging.error(f"Usage: python3 client.py <IPReg>:<portReg> <IPProxy>:<portProxy> <addrClient> <addrServerRTP> <time> <file> - {e}")
        raise ERROR_MESSAGE


# SIP METHODS FUNCTIONS
def client_invite_body(dir_server_rtp, dir_client):
    method = f"INVITE {dir_server_rtp} {SIP_CONSTANT}\r\n"
    head = "Content-Type: application/sdp\r\n"
    body = f"\nv=0\n" \
           f"o={dir_client} 127.0.0.1\n" \
           f"s={dir_server_rtp.split('@')[0].split(':')[1]}\n" \
           f"t=0\n" \
           f"m=audio {CLIENT_AS_SERVER_PORT} RTP"
    length = f"Content-Length: {len(body)}\r\n"
    return method + head + length + body


def client_ack_body(dir_server_rtp, dir_client):
    method = f"ACK {dir_server_rtp} {SIP_CONSTANT}\r\n"
    return method


def client_bye_body(dir_server_rtp, dir_client):
    method = f"BYE {dir_server_rtp} {SIP_CONSTANT}\r\n"
    return method


SIP_REQUEST = {
    "INVITE": client_invite_body,
    "ACK": client_ack_body,
    "BYE": client_bye_body
}


def register_client_in_sip(register_server_ip, register_server_port, dir_client):
    try:
        logging.debug("Register Client in Register Server - Init")
        client_request = f"{REGISTER_COMMAND} {dir_client} {SIP_CONSTANT}\r\n\r\n"
        logging.debug(f"Client Register Request: {client_request}")
        # connection to sip server
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 2)
            logging.debug(f"RTP Server set ip and port: {IP_SERVER, CLIENT_AS_SERVER_PORT}")
            client_socket.bind((IP_SERVER, CLIENT_AS_SERVER_PORT))
            client_socket.connect_ex((register_server_ip, register_server_port))
            client_socket.send(client_request.encode(ENCODING))
            logging.info(f"SIP to {register_server_ip}:{register_server_port}: {client_request}")
            register_server_response = client_socket.recv(1024).decode(ENCODING)
            logging.info(f"SIP from {register_server_ip}:{register_server_port}: {register_server_response}")
            logging.debug("Register RTP in Register Server - Finish Without Error")
            return register_server_response
    except Exception as e:
        logging.error(f"Register Client in Register Server - Finish With Error - {e}")
        raise e


def invite_client_request(client_dir, rtp_server_dir, proxy_server_ip, proxy_server_port):
    try:
        logging.debug("Invite Client Request - Init")
        invite_client_request: str = SIP_REQUEST[CLIENT_SERVER_SIP_METHODS[0]](rtp_server_dir, client_dir)
        logging.debug(f"Invite Client Request: \n{invite_client_request}")
        # connection to sip server
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            client_socket.connect_ex((proxy_server_ip, proxy_server_port)) #conexion al proxy
            logging.info(f"SIP to {proxy_server_ip}:{proxy_server_port}: {invite_client_request}")
            client_socket.send(invite_client_request.encode(ENCODING))
            proxy_server_response = client_socket.recv(1024).decode(ENCODING)
            logging.info(f"SIP from {proxy_server_ip}:{proxy_server_port}: {proxy_server_response}")
            client_socket.close()
            logging.debug("Invite Client Request - Finish Without Error")
            return proxy_server_response
    except Exception as e:
        logging.error(f"Invite Client Request - Finish With Error - {e}")
        raise e


def ack_client_request(client_dir, rtp_server_dir, proxy_server_ip, proxy_server_port):
    try:
        logging.debug("Ack Client Request - Init")
        ack_client_request: str = SIP_REQUEST[CLIENT_SERVER_SIP_METHODS[1]](rtp_server_dir, client_dir)
        logging.debug(f"Ack Client Request: {invite_client_request}")
        # connection to sip server
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            client_socket.bind((IP_SERVER, CLIENT_AS_SERVER_PORT))
            client_socket.connect_ex((proxy_server_ip, proxy_server_port))
            logging.info(f"SIP to {proxy_server_ip}:{proxy_server_port}: {ack_client_request}")
            client_socket.send(ack_client_request.encode(ENCODING))
            proxy_server_response = client_socket.recv(1024).decode("utf-8")
            logging.info(f"SIP from {proxy_server_ip}:{proxy_server_port}: {proxy_server_response}")
            logging.debug("Ack Client Request - Finish Without Error")
            return proxy_server_response
    except Exception as e:
        logging.error(f"Ack Client Request - Finish With Error - {e}")
        raise e


def bye_client_request(client_dir, rtp_server_dir, proxy_server_ip, proxy_server_port, client_server):
    try:
        logging.debug("Bye Client Request - Init")
        client_request: str = SIP_REQUEST[CLIENT_SERVER_SIP_METHODS[2]](rtp_server_dir, client_dir)
        logging.debug(f"Bye Client Request: {client_request}")
        # connection to sip server
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as client_socket:
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            client_socket.connect_ex((proxy_server_ip, proxy_server_port))
            logging.info(f"SIP to {proxy_server_ip}:{proxy_server_port}: {client_request}")
            client_socket.send(client_request.encode(ENCODING))
            proxy_server_response = client_socket.recv(1024).decode(ENCODING)
            logging.info(f"SIP from {proxy_server_ip}:{proxy_server_port}: {proxy_server_response}")
        client_server.server_close()
        logging.debug("Bye Client Request - Finish Without Error")
    except Exception as e:
        logging.error(f"Bye Client Request - Finish With Error - {e}")
        raise e


def write_in_audio_file(file_name: str, content): #recive fichero audio
    try:
        logging.debug("Write in Audio File - Init")
        with open(file_name, "a+b") as f:
            f.write(content)
        logging.debug("Write in Audio File - Finish Without Error")
    except Exception as e:
        logging.error(f"Write in Audio File - Finish With Error - {e}")
        raise e


def get_audio_file_name():
    try:
        logging.debug("Get Audio File Name - Init")
        register_server_ip, register_server_port, server_sip_ip, server_sip_port, dir_client, dir_server_rtp, time_expire, audio_file = get_application_parameters()
        audio_file_name = dir_server_rtp.split(":")[1].split("@")[0] #cogemos el nombre de la cancion de un parámetro de la aplicación
        logging.debug(f"Audio File Name: {audio_file}")
        logging.debug("Get Audio File Name - Finish Without Error")
        return audio_file_name
    except Exception as e:
        logging.error(f"Get Audio File Name - Finish With Error - {e}")
        raise e


class ClientServerHandler(socketserver.BaseRequestHandler):

    audio_file = ""

    def handle(self):
        try:
            logging.debug("ClientServerHandler - Handle - Init")
            server_ip = self.client_address[0]
            server_port = self.client_address[1]
            msg = self.request[0]
            rtp_packages = msg[12:] #posicion 12 hasta el final
            logging.debug(f"RTP packages from : {server_ip}:{server_port} -> {rtp_packages}")
            write_in_audio_file(self.audio_file, rtp_packages)
            logging.debug("ClientServerHandler - Handle - Finish Without Error")
        except Exception as e:
            logging.error(f"RTPServerHandler - Handle - Finish With Error - {e}")
            raise e


if __name__ == '__main__':
    try:
        logging.info(f"Starting...")
        logging.debug(f"===========Client As Client Starting =============")
        register_server_ip, register_server_port, server_sip_ip, server_sip_port, dir_client, dir_server_rtp, time_expire, audio_file = get_application_parameters()
        # Client as Client ( register )
        proxy_server_response = register_client_in_sip(register_server_ip, register_server_port, dir_client) #in register***
        # client as client ( invite )
        if "200" in proxy_server_response:
            proxy_server_response = invite_client_request(dir_client, dir_server_rtp, server_sip_ip, server_sip_port)
            # client as client ( ack )
            if "200" in proxy_server_response:
                proxy_server_response = ack_client_request(dir_client, dir_server_rtp, server_sip_ip, server_sip_port)
            else:
                raise f"{CLIENT_SERVER_SIP_METHODS[1]} Method request incorrect"
        else:
            raise f"{CLIENT_SERVER_SIP_METHODS[0]} Method request incorrect"
        logging.debug(f"===========Client As Client Finish =============")

        # client as server (bye)
        logging.debug(f"===========Client As Server Starting =============") #se convierte cliente en serv para recibir el audio
        ClientServerHandler.audio_file = audio_file
        client_as_server = socketserver.UDPServer((IP_SERVER, CLIENT_AS_SERVER_PORT), ClientServerHandler)
        threading.Thread(target=client_as_server.serve_forever).start()
        logging.debug(f"===========Client As Server Listening in {IP_SERVER}:{CLIENT_AS_SERVER_PORT} =============")
        logging.info(f"RTP ready {CLIENT_AS_SERVER_PORT}")

        # client as client ( bye )
        threading.Timer(
            time_expire,
            bye_client_request,
            kwargs={ #paso argumentos como diccionario
                "client_dir": dir_client,
                "rtp_server_dir": dir_server_rtp,
                "proxy_server_ip": server_sip_ip,
                "proxy_server_port": server_sip_port,
                "client_server": client_as_server
            }
        ).start()
    except Exception as e:
        logging.error(f"Client Error: {e}")
        sys.exit()

