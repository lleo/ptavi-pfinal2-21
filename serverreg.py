#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Register Server

python3 serverreg.py <puerto> <fichero>
==================================================================
• <puerto> será el puerto UDP donde recibirá peticiones SIP tanto de clientes como de servidores RTP.
• <fichero> será el fichero JSON donde se guardarán los datos de todos los que se registren.

Example: python3 serverreg.py 6001 register.json
"""
import json
import logging
import socketserver
import sys

# CONSTANTS
IP_SERVER = "127.0.0.1"
ERROR_MESSAGE = "Usage: python3 serverreg.py <port> <file>"
ENCODING = "UTF-8"
LOGGING_FMT = '%(asctime)s-%(message)s'
LOGGING_DATE_FMT = '%Y%m%d%H%M%S'

# SIP METHODS
REGISTER_SERVER_SIP_METHODS = ["REGISTER"]

# CODE RESPONSES
CODES = {
    200: b"SIP/2.0 200 OK",
    400: b"SIP/2.0 400 Bad Request",
    405: b"SIP/2.0 405 Method Not Allowed"
}

logging.basicConfig(level=logging.INFO, format=LOGGING_FMT, datefmt=LOGGING_DATE_FMT)


def get_application_parameters():
    try:
        logging.debug("Get Application Parameters - Init")
        register_server_port = sys.argv[1]
        audio_file = sys.argv[2]
        logging.debug(f"Application Parameters: {register_server_port=}, {audio_file=}")
        logging.debug("Get Application Parameters - Finish Without Error")
        return int(register_server_port), audio_file
    except Exception as e:
        logging.error(f"Usage: python3 serverreg.py <port> <file> - {e}")
        raise ERROR_MESSAGE


def find_hist_register_data(register_file: str):
    try:
        logging.debug("Find History Register Data - Init")
        RegisterServerHandler.register_file = register_file
        with open(register_file, encoding=ENCODING) as rf: #abro fichero
            register_data = json.load(rf) #cargo fichero
            RegisterServerHandler.register_data = register_data #cogemos datos del fichero
        logging.debug(f"User history found: {register_data}")
        logging.debug("Find History Register Data - Finish Without Error")
    except Exception as e:
        logging.debug(f"Find History Register Data - Finish With Error - {e}")


def write_in_json_file(json_file: str, content: dict):
    try:
        logging.debug("Write In Json File - Init")
        with open(json_file, "w") as f:
            logging.debug(f"Register in {json_file}: {content} ")
            json.dump(content, f)
        logging.debug("Write In Json File - Finish Without Error")
    except Exception as e:
        logging.error(f"Write In Json File - Finish With Error- {e}")
        raise e


class RegisterServerHandler(socketserver.DatagramRequestHandler):
    # atributo de clase
    register_data = {}
    register_file = ""

    def handle(self):
        try:
            logging.debug("RegisterServerHandler - Handle - Init")
            sip_request = self.rfile.read().decode(ENCODING)
            sip_request_ip = self.client_address[0]
            sip_request_port = self.client_address[1]
            logging.debug(f"Start connection to {sip_request_ip}:{sip_request_port}")
            logging.debug(f"Request: {sip_request}")
            if REGISTER_SERVER_SIP_METHODS[0] in sip_request:  # REGISTER
                logging.info(f"SIP from {sip_request_ip}:{sip_request_port} {sip_request}")
                logging.debug(f"Method: {REGISTER_SERVER_SIP_METHODS[0]}")
                request_address = sip_request.split()[1].split(":")[1]
                logging.debug(f"Request Address: {request_address}")
                self.register_data[request_address] = (sip_request_ip, sip_request_port)
                write_in_json_file(self.register_file, self.register_data)
                logging.debug(f"All Register: {self.register_data}")
                server_response = CODES[200]
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port} {server_response.decode(ENCODING)}") #cambio de binario a string
                self.wfile.write(server_response)
                logging.debug(f"SIP to {sip_request_ip}:{sip_request_port}: {server_response.decode(ENCODING)}")
            else:
                logging.error(f"Method not Register: Valid Methods {REGISTER_SERVER_SIP_METHODS}")
                server_response = CODES[405]
                self.wfile.write(server_response)
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {repr(server_response)}")
            logging.debug(f"Finish connection to {sip_request_ip}:{sip_request_port}")
            logging.debug("RegisterServerHandler - Handle - Finish Without Error")
        except Exception as e:
            logging.error(f"RegisterServerHandler - Handle - Finish With Error - {e}")
            self.wfile.write(CODES[405])
            raise Exception(e)


if __name__ == "__main__":
    try:
        logging.info(f"Starting...")
        logging.debug(f"===========Register Server Starting=============")
        register_server_port, register_file = get_application_parameters()
        find_hist_register_data(register_file)
        serv = socketserver.UDPServer((IP_SERVER, register_server_port), RegisterServerHandler)
        logging.debug(f"===========Register Server Listening in {IP_SERVER}:{register_server_port}=============")
        serv.serve_forever()
    except Exception as e:
        logging.error(f"Register Server Error: {e}")
        sys.exit()
