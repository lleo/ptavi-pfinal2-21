#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" RTP Server

python3 serverrtp.py <IPReg>:<puertoReg> <fichero>
==================================================================
• <IPReg> es la dirección IP de la máquina donde está ServidorRegistrar
• <puertoReg> es el puerto UDP donde está escuchando ServidorRegistrar
• <fichero> será el fichero con el sonido, que se enviará mediante RTP cuando se establezca una sesión con un cliente

Example: python3 serverrtp.py 127.0.0.1:6001 heyjude.mp3
"""
import logging
import os
import socket
import socketserver
import sys
import time

import simplertp

# CONSTANTS
IP_SERVER = "127.0.0.1"
ERROR_MESSAGE = "Usage: python3 serverrtp.py <IPReg>:<portReg> <file>"
ENCODING = "UTF-8"
LOGGING_FMT = '%(asctime)s-%(message)s'
LOGGING_DATE_FMT = '%Y%m%d%H%M%S'
RTP_DOMAIN = "@signasong.net"
PORT_SERVER = 9999
SIP_CONSTANT = "SIP/2.0"
REGISTER_COMMAND = "REGISTER"

# SIP METHODS
RTP_SERVER_SIP_METHODS = ["INVITE", "ACK", "BYE"]

# CODE RESPONSES
CODES = {
    200: b"SIP/2.0 200 OK",
    400: b"SIP/2.0 400 Bad Request",
    405: b"SIP/2.0 405 Method Not Allowed"
}

logging.basicConfig(level=logging.INFO, format='%(asctime)s-%(message)s', datefmt='%Y%m%d%H%M%S')


def find_audio_file(audio_file, path):
    try:
        logging.debug("Find Audio File - Init")
        for root, dirs, files in os.walk(path): #buscamos fichero audio
            logging.debug(f"Files in {path=}: {files=}")
            if audio_file in files:
                logging.debug("Find Audio File - Finish Without Error")
                return True
            else:
                raise Exception(f"Audio file: {audio_file} in path {path} not found")
        logging.debug("Find Audio File - Finish Without Error")
    except Exception as e:
        logging.error(f"Find Audio File - Finish With Error - {e}")
        raise e


def get_application_parameters():
    try:
        logging.debug("Get Application Parameters - Init")
        register_server_ip, register_server_port = sys.argv[1].split(":")
        audio_file = sys.argv[2]
        logging.debug(f"Application Parameters: {register_server_port=}, {audio_file=}")
        find_audio_file(audio_file, os.getcwd()) #directorio de trabajo actual
        logging.debug("Get Application Parameters - Finish Without Error")
        return register_server_ip, int(register_server_port), audio_file
    except Exception as e:
        logging.error(f"Usage: python3 serverrtp.py <IPReg>:<portReg> <file> - {e}")
        raise ERROR_MESSAGE


def register_rtp_in_register_server(register_server_ip, register_server_port, audio_file):
    try:
        logging.debug("Register RTP in Register Server - Init")
        rtp_address_to_register_in_sip = f"sip:{audio_file.split('.')[0]}{RTP_DOMAIN}"
        rtp_request = f"{REGISTER_COMMAND} {rtp_address_to_register_in_sip} {SIP_CONSTANT}\r\n\r\n"
        logging.debug(f"RTP Register Request: {rtp_request}")
        # connection to register server
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as rtp_socket: #abro conexion con puerto e ip del serv register
            rtp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            logging.debug(f"RTP Server set ip and port: {IP_SERVER, PORT_SERVER}")
            rtp_socket.bind((IP_SERVER, PORT_SERVER))
            rtp_socket.connect_ex((register_server_ip, register_server_port)) #conexion con ip y puerto
            logging.info(f"SIP to {register_server_ip}:{register_server_port}: {rtp_request}")
            rtp_socket.send(rtp_request.encode(ENCODING)) #envío info en byte
            register_server_response = rtp_socket.recv(1024).decode(ENCODING) #recivo info string
            logging.info(f"SIP from {register_server_ip}:{register_server_port}: {register_server_response}")
            rtp_socket.close()
            logging.debug("Register RTP in Register Server - Finish Without Error")
            return register_server_response
    except Exception as e:
        logging.error(f"Register RTP in Register Server - Finish With Error - {e}")
        raise e


def get_document_sdp_body_parameters(sip_document_sdp_body):
    try:
        logging.debug("Get Document SDP body Parameters - Init")
        version, origin, session, time_session, extra = sip_document_sdp_body.split("\n")[1:]
        session = session.split("=")[1]
        version = int(version.split("=")[1])
        client_address, client_ip = origin.split()
        client_address = client_address.split(":")[1]
        time_session = int(time_session.split("=")[1])
        type_element, client_port, connection_type = extra.split()
        logging.debug(f"{client_address=}, {client_ip=}, {client_port=}, {session=}")
        logging.debug("Get Document SDP body Parameters - Finish Without Error")
        return client_address, client_ip, int(client_port), session
    except Exception as e:
        logging.error(f"Get Document SDP body Parameters - Finish With Error - {e}")
        raise e


def send_audio_file(client_ip, client_port, audio_file):
    try:
        logging.debug(f"Send Audio File - Init")
        logging.info(f"RTP to {client_ip}:{client_port}")
        sender = simplertp.RTPSender(ip=client_ip, port=client_port, file=audio_file, printout=True)
        sender.send_threaded()
        logging.debug(f"Send Audio File - Finish Without Error")
        RTPServerHandler.sender = sender
    except Exception as e:
        logging.error(f"Send Audio File - Finish With Error - {e}")
        raise e


class RTPServerHandler(socketserver.DatagramRequestHandler):
    sender = None
    session = None
    client_ip = None
    client_port = None

    def handle(self):
        try:
            logging.debug("RTPServerHandler - Handle - Init")
            sip_request = self.rfile.read().decode(ENCODING)
            sip_request_ip = self.client_address[0]
            sip_request_port = self.client_address[1]
            logging.debug(f"Start connection to {sip_request_ip}:{sip_request_port}")
            logging.debug(f"Request: {sip_request}")
            if RTP_SERVER_SIP_METHODS[0] in sip_request:  # INVITE
                logging.info(f"SIP from {sip_request_ip}:{sip_request_port} {sip_request}")
                logging.debug(f"Method: {RTP_SERVER_SIP_METHODS[0]}")
                method, head, length, body = sip_request.split("\r\n")
                client_address, RTPServerHandler.client_ip, RTPServerHandler.client_port, session = get_document_sdp_body_parameters(body) #asigno los valores del sdp
                server_response = CODES[200]
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {server_response.decode(ENCODING)}")
                self.wfile.write(server_response) #envio 200 ok
                RTPServerHandler.session = session
            elif RTP_SERVER_SIP_METHODS[1] in sip_request:  # ACK
                logging.info(f"SIP from {sip_request_ip}:{sip_request_port} {sip_request}")
                logging.debug(f"Method: {RTP_SERVER_SIP_METHODS[1]}")
                audio_file = RTPServerHandler.session + ".mp3" #sesion del invite y .mp3
                send_audio_file(RTPServerHandler.client_ip, RTPServerHandler.client_port, audio_file)
            elif RTP_SERVER_SIP_METHODS[2] in sip_request:  # BYE
                logging.info(f"SIP from {sip_request_ip}:{sip_request_port} {sip_request}")
                logging.debug(f"Method: {RTP_SERVER_SIP_METHODS[1]}")
                self.sender.finish() #finaliza el envío
                logging.debug("Finish RTP Package")
                server_response = CODES[200]
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {server_response.decode(ENCODING)}")
                self.wfile.write(server_response)
                logging.debug(f"Interruption of sending RTP packets to client")
            else:
                logging.error(f"Method not Register: Valid Methods {RTP_SERVER_SIP_METHODS}")
                server_response = CODES[405]
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {server_response.decode(ENCODING)}")
                self.wfile.write(server_response)
            logging.debug(f"Finish connection to {sip_request_ip}:{sip_request_port}")
            logging.debug("RTPServerHandler - Handle - Finish Without Error")
        except Exception as e:
            logging.debug(f"RTPServerHandler - Handle - Finish With Error - {e}")
            self.wfile.write(CODES[405])
            raise e


if __name__ == '__main__':
    try:
        # RTP Client ( register )
        logging.info(f"Starting...")
        logging.debug(f"===========RTP Server Starting=============")
        server_register_ip, server_register_port, file_audio = get_application_parameters()
        sip_response = register_rtp_in_register_server(server_register_ip, server_register_port, file_audio)
        if "200" in sip_response:
            # Server RTP and RTP Client
            serv = socketserver.UDPServer((server_register_ip, PORT_SERVER), RTPServerHandler)
            logging.debug(f"===========RTP Server Listening in {IP_SERVER}:{PORT_SERVER}=============")
            serv.serve_forever()
        else:
            raise sip_response
    except Exception as e:
        sys.exit(f"RTP Server Error: {e}")
