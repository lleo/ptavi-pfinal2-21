#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Proxy Server

python3 serverproxy.py <puerto> <fichero>
==================================================================
• <puerto> será el puerto UDP donde recibirá peticiones SIP tanto de clientes como de servidores RTP.
• <fichero> será el fichero JSON donde se leerán los datos de los que se han registrado con ServidorRegistrar.

Example: python3 serverproxy.py 6004 register.json
"""
import json
import logging
import socket
import socketserver
import sys

# CONSTANTS
IP_SERVER = "127.0.0.1"
ERROR_MESSAGE = "Usage: python3 serverrtp.py <port> <file>"
ENCODING = "UTF-8"
LOGGING_FMT = '%(asctime)s-%(message)s'
LOGGING_DATE_FMT = '%Y%m%d%H%M%S'

# SIP METHODS
PROXY_SERVER_SIP_METHODS = ["INVITE", "ACK", "BYE"]

# CODE RESPONSES
CODES = {
    200: b"SIP/2.0 200 OK",
    400: b"SIP/2.0 400 Bad Request",
    405: b"SIP/2.0 405 Method Not Allowed"
}

logging.basicConfig(level=logging.INFO, format=LOGGING_FMT, datefmt=LOGGING_DATE_FMT)


def get_application_parameters():
    try:
        logging.debug("Get Application Parameters - Init")
        register_server_port = sys.argv[1]
        file = sys.argv[2]
        logging.debug(f"Application Parameters: {register_server_port=}, {file=}")
        logging.debug("Get Application Parameters - Finish Without Error")
        return int(register_server_port), file
    except Exception as e:
        logging.error(f"Usage: python3 serverrtp.py <port> <file> - {e}")
        raise ERROR_MESSAGE


def find_hist_register_data(register_file: str):
    try:
        logging.debug("Find History Register Data - Init")
        ProxyServerHandler.register_file = register_file
        with open(register_file, encoding=ENCODING) as rf: #abro fichero
            register_data = json.load(rf) #cargo fichero
            ProxyServerHandler.register_data = register_data #cogemos datos del fichero
            logging.debug(f"User history found: {register_data}")
            logging.debug("Find History Register Data - Finish Without Error")
            return register_data
    except Exception as e:
        logging.error(f"Find History Register Data - Finish With Error - {e}")


def sip_proxy(ip, port, request):
    try:
        logging.debug("Sip Proxy - Init")
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sip_socket: #abrimos conexion
            sip_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sip_socket.connect((ip, port))
            logging.debug(f"Proxy to {ip}:{port}: {request}")
            logging.info(f"SIP to {ip}:{port} {request}")
            sip_socket.send(request.encode(ENCODING)) #envío de info en byte (de string a byte)
            response = sip_socket.recv(1024).decode(ENCODING) #recogemos info en string (byte a string)
            logging.debug(f"Proxy from {ip}:{port}: {response}")
            logging.info(f"SIP from {ip}:{port} {response}")
            logging.debug("Sip Proxy - Finish Without Error")
            return response
    except Exception as e:
        logging.error(f"Sip Proxy - Finish With Error - {e}")
        raise e


class ProxyServerHandler(socketserver.DatagramRequestHandler):
    # atributo de la clase
    register_data = {} #diccionario
    register_file = "" #string

    def handle(self):
        try:
            logging.debug("ProxyServerHandler - Handle - Init")
            sip_request = self.rfile.read().decode(ENCODING) #recibe info
            sip_request_ip = self.client_address[0] #ip de quien me envía info
            sip_request_port = self.client_address[1] #puerto de quien me envía info
            logging.debug(f"Start connection to {sip_request_ip}:{sip_request_port}")
            logging.info(f"Request: \n{sip_request}")
            if PROXY_SERVER_SIP_METHODS[0] in sip_request:  # INVITE
                logging.info(f"SIP from {sip_request_ip}:{sip_request_port} {sip_request}")
                logging.debug(f"Method: {PROXY_SERVER_SIP_METHODS[0]}")
                method, head, length, body = sip_request.split("\r\n")
                method, server_rtp_address, sip_constant = method.split()
                server_rtp_address = server_rtp_address.split(":")[1]
                logging.debug(f"Server RTP Address: {server_rtp_address}")
                server_rtp_ip, server_rtp_port = find_hist_register_data(self.register_file)[server_rtp_address] #cojo info del servidor a enviar del fich
                logging.debug(f"Server RTP Address: {server_rtp_address} -> {server_rtp_ip, server_rtp_port}")
                proxy_response = sip_proxy(server_rtp_ip, server_rtp_port, sip_request)
                self.wfile.write(proxy_response.encode(ENCODING))
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {proxy_response}")
            elif PROXY_SERVER_SIP_METHODS[1] in sip_request:  # ACK
                logging.info(f"SIP from {sip_request_ip}:{sip_request_port} {sip_request}")
                logging.debug(f"Method: {PROXY_SERVER_SIP_METHODS[1]}")
                method, server_rtp_address, sip_constant = sip_request.split()
                server_rtp_address = server_rtp_address.split(":")[1]
                logging.debug(f"Server RTP Address: {server_rtp_address}")
                server_rtp_ip, server_rtp_port = find_hist_register_data(self.register_file)[server_rtp_address]
                logging.debug(f"Server RTP Address: {server_rtp_address} -> {server_rtp_ip, server_rtp_port}")
                proxy_response = sip_proxy(server_rtp_ip, server_rtp_port, sip_request)
                self.wfile.write(proxy_response.encode(ENCODING))
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {proxy_response}")
            elif PROXY_SERVER_SIP_METHODS[2] in sip_request:  # BYE
                logging.info(f"SIP from {sip_request_ip}:{sip_request_port} {sip_request}")
                logging.debug(f"Method: {PROXY_SERVER_SIP_METHODS[2]}")
                method, server_rtp_address, sip_constant = sip_request.split()
                server_rtp_address = server_rtp_address.split(":")[1]
                logging.debug(f"Server RTP Address: {server_rtp_address}")
                server_rtp_ip, server_rtp_port = find_hist_register_data(self.register_file)[server_rtp_address]
                logging.debug(f"Server RTP Address: {server_rtp_address} -> {server_rtp_ip, server_rtp_port}")
                proxy_response = sip_proxy(server_rtp_ip, server_rtp_port, sip_request)
                self.wfile.write(proxy_response.encode(ENCODING))
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {proxy_response}")
            else:
                logging.error(f"Method not Register: Valid Methods {PROXY_SERVER_SIP_METHODS}")
                server_response = CODES[405]
                logging.info(f"SIP to {sip_request_ip}:{sip_request_port}: {server_response.decode(ENCODING)}")
                self.wfile.write(server_response)
            logging.debug(f"Finish connection to {sip_request_ip}:{sip_request_port}")
            logging.debug("ProxyServerHandler - Handle - Finish Without Error")
        except Exception as e:
            logging.error(f"ProxyServerHandler - Handle - Finish With Error - {e}")
            self.wfile.write(CODES[405])



if __name__ == "__main__":
    try:
        logging.info(f"Starting...")
        logging.debug(f"===========Proxy Server Starting=============")
        proxy_server_port, register_file = get_application_parameters()
        find_hist_register_data(register_file)
        serv = socketserver.UDPServer((IP_SERVER, proxy_server_port), ProxyServerHandler) #inciamos servidor
        logging.debug(f"===========Proxy Server Listening in {IP_SERVER}:{proxy_server_port}=============\n")
        serv.serve_forever()
    except Exception as e:
        logging.error(f"Proxy Server Error: {e}")
        sys.exit()
