## Parte básica 

1. Lanzamos ServidorRegistrar: **serverreg.py 6001 register.json**

![](capturasdoc/uno.png)
2. Lanzamos ServerProxy: **serverproxy.py 6004 register.json**

![](capturasdoc/dos.png)
3. Lanzamos ServerRTP: **serverrtp.py 127.0.0.1:6001 cancion.mp3**

Envía un REGISTER al ServidorRegistrar y éste le responde con 200 OK

![](capturasdoc/tres.png)
![](capturasdoc/cuatro.png)

4. Lanzamos Client: **127.0.0.1:6001 127.0.0.1:6004 sip:yo@clientes.net sip:cancion@signasong.net 10 recived.mp3**

Se registra en ServeridorRegistrar
![](capturasdoc/cinco.png)

Envía INVITE al proxy con documento SDP 
![](capturasdoc/seis.png)

Si todo es correcto, el cliente manda ACK y se queda escuchando para recir los paquetes RTP
![](capturasdoc/siete.png)

ServerRTP comienza en envío de paquetes
![](capturasdoc/ocho.png) 

Hasta que el cliente envíe un bye
![](capturasdoc/nueve.png)

Lo recibe el ServerRTP y se finaliza el intercambio de audio
![](capturasdoc/diez.png)

### Ejemplo impresión por pantalla al no introducir correctamente los parámetros en la llamada

![](capturasdoc/captura.png)


## Parte adicional 
* 1. Cabecera de tamaño
![](capturasdoc/tamaño.png)

* 2. Gestión de errores

    Se puede comprobar de las siguientes maneras:

-Introduciendo un fichero .json que no existe en la llamada del Proxy: **serverproxy.py 6004 noexiste.json**

![](capturasdoc/error1.png)

![](capturasdoc/error2.png)

-Introduciendo una canción que no existe en Client: **client.py 127.0.0.1:6001 127.0.0.1:6004 sip:yob@clientes.net sip:cancionnoexiste@signasong.net 10 recived2b.mp3**

![](capturasdoc/error3.png)

-Introduciendo una canción que no existe en el RTP: **serverrtp.py 127.0.0.1:6001 otracancion.mp3**

![](capturasdoc/error4.png)

-Introduciendo un puerto erróneo en Proxy: **serverproxy.py 7004 register.json**

![](capturasdoc/error5.png)


